﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queue
{
    public class DemoQueue
    {
        public int[] items;
        int top;
        int bot = -1;
        int maxQueue;

        public DemoQueue(int maxQueue)
        {
            this.maxQueue = maxQueue;
            items = new int[maxQueue];
            top = 0;
        }

        public void EnQueue(int item)
        {
            if (bot == maxQueue -1)
            {
                throw new Exception("Queue is full");
            }
            else
            {
                bot++;
                items[bot] = item;
            }
        }

        public int DeQueue()
        {
            if (bot == -1)
            {
                throw new Exception("Queue is empty");
            }
            if(top == bot)
            {
                int a = items[top];
                top = 0;
                bot = -1;
                return a;
            }
            else
            {
                int valueAtTop = items[top];
                top++;
                return valueAtTop;
            }
        }

        public int Peek()
        {
            if (bot == -1)
            {
                throw new Exception("Queue is empty");
            }
            else
            {
                int valueAtTop = items[top];
                return valueAtTop;
            }
        }
        public int Count()=> bot - top + 1;
    }
}
