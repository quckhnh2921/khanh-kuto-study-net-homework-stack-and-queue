﻿using Queue;

try
{
    DemoQueue queue = new DemoQueue(5);
    //add item into queue
    queue.EnQueue(3);
    queue.EnQueue(1);
    queue.EnQueue(2);
    queue.EnQueue(6);
    queue.EnQueue(9);


    //dequeue will take the element which add first into the queue out of the queue
    //in this case dequeue will take 3
    Console.WriteLine("Dequeue: " + queue.DeQueue());
    //in this case dequeue will take 1
    Console.WriteLine("Dequeue: " + queue.DeQueue());
    Console.WriteLine("Dequeue: " + queue.DeQueue());
    Console.WriteLine("Dequeue: " + queue.DeQueue());
    Console.WriteLine("Dequeue: " + queue.DeQueue());
    queue.EnQueue(9);
    Console.WriteLine("Dequeue: " + queue.DeQueue());


    Console.WriteLine("Value at top without remove: " + queue.Peek());
    Console.WriteLine("Count element: " + queue.Count());
    //=>all of this step is FIFO
}catch(Exception ex)
{
    Console.WriteLine(ex.Message);
}