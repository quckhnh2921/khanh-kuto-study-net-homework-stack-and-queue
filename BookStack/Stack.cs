﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStack
{
    public class Stack
    {
        Book[] book;
        int maxStack;
        int top = -1;
        int count = 0;
         
        public Stack(int maxStack)
        {
            this.maxStack = maxStack;
            book = new Book[maxStack];  
        }

        public void AddBookIntoStack(string bookName, string author)
        {
            if(top == maxStack)
            {
                throw new Exception("Stack is full");
            }
            else
            {
                top++;
                book[top] = new Book();
                book[top].author = author;
                book[top].bookName = bookName;
            }
        }
        public string RemoveBookFromTop()
        {
            if(top == -1)
            {
                throw new Exception("Stack is empty");
            }
            else
            {
                string nameOfBookRemoved = book[top].bookName;
                string author = book[top].author;
                top--;
                return "Name: "+nameOfBookRemoved + "| Author: "+author ;
            }
        }

        public string BookAtTopDetails()
        {
            if(top == -1)
            {
                throw new Exception("Stack is empty");
            }
            else
            {
                string bookName = book[top].bookName;
                string author = book[top].author;
                return "Name: " + bookName + "| Author: " + author;
            }
        }

        public int NumberOfBookInStack()
        {
            return count = top+1;
        }
    }
}
