﻿using BookStack;
int choice = 0;
Console.Write("Enter book stack: ");
int maxStack = int.Parse(Console.ReadLine());
Stack stack = new Stack(maxStack);
do
{
    Console.WriteLine("------------------------------");
    Console.WriteLine("|  1. Add book into stack    |");
    Console.WriteLine("|  2. Remove book from stack |");
    Console.WriteLine("|  3. Details of book at top |");
    Console.WriteLine("|  4. Number of book in stack|");
    Console.WriteLine("|  5. Exit !                 |");
    Console.WriteLine("------------------------------");
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            Console.Write("Add book name: ");
            string bookName = Console.ReadLine();
            Console.Write("Add author: ");
            string author = Console.ReadLine();
            stack.AddBookIntoStack(bookName, author);
            Console.WriteLine("Done");
            break;
        case 2:
            Console.WriteLine("Remove: " + stack.RemoveBookFromTop());

            break;
        case 3:
            Console.WriteLine("Details of book at top position: " + stack.BookAtTopDetails());

            break;
        case 4:
            Console.WriteLine("Number of book: " + stack.NumberOfBookInStack());

            break;
        case 5:
            Environment.Exit(0);
            break;
        default:
            Console.WriteLine("Invalid choice");
            break;
    }
} while (choice != 5);