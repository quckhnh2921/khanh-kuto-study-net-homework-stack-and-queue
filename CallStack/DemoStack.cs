﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallStack
{
    public class DemoStack
    {
        public int[] items;
        int top = -1;
        int maxStack;
        public DemoStack(int _maxStack)
        {
            maxStack = _maxStack;
            items = new int[maxStack];
        }

        public void Push(int item)
        {
            if(isFull())
            {
                Console.WriteLine("Stack is full");
            }
            else
            {
                top++;
                items[top] = item;
            }
        }

        public int Pop()
        {
            if(isEmpty())
            {
                throw new Exception("Stack is empty");
            }
            else
            {
                int valueAtTop = items[top];
                top--;
                return valueAtTop;
            }
        }

        public int Top()
        {
            if(isEmpty())
            {
                throw new Exception("Stack is empty");
            }
            else
            {
                int valueAtTop = items[top];
                return valueAtTop;
            }
        }
        public bool isEmpty()
        {
            return top == -1;
        }

        public bool isFull()
        {
            return top == maxStack;
        }

        public int Count()
        {
            return top + 1;
        }
    }
}
