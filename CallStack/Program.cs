﻿using CallStack;

DemoStack demo = new DemoStack(5);
demo.Push(1);
demo.Push(2);
demo.Push(4);
demo.Push(5);
demo.Push(3);
Console.WriteLine(demo.Pop());
Console.WriteLine(demo.Pop());
Console.WriteLine(demo.Pop());
Console.WriteLine("Top index value: "+demo.Top());
Console.WriteLine("Count: "+ demo.Count());
